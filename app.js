var express = require('express');
var app = express();
app.use(express.static('public'));
app.use(express.static('dashboard'));
var http = require('http').Server(app);
var io = require('socket.io')(http);
var readline = require('readline');
var port = process.env.PORT || 3000;
var util = require('util');

var lobbyUsers = {};
var users = {};
var activeGames = {};

console.reset = function () {
    return process.stdout.write('\033c');
}

app.get('/', function(req, res) {
 res.sendFile(__dirname + '/public/default.html');

});

app.get('/userlist/', function(req, res) {
 res.sendFile(__dirname + '/userlist/userlist.html');
});

app.get('/dashboard/', function(req, res) {
    res.sendFile(__dirname + '/dashboard/dashboard.html');
});

io.on('connection', function(socket) {
    console.log('new connection ' + socket);
    
    socket.on('login', function(userId) {
       doLogin(socket, userId);
    });

    function doLogin(socket, userId) {
        socket.userId = userId;  
     
        if (!users[userId]) {    
            console.log('creating new user '+socket.userId);
            users[userId] = {userId: socket.userId, games:{}};
        } else {
            console.log('user found!');
            Object.keys(users[userId].games).forEach(function(gameId) {
                console.log('gameid - ' + gameId);
            });
        }
        
        socket.emit('login', {users: Object.keys(lobbyUsers), 
                              games: Object.keys(users[userId].games)});
        lobbyUsers[userId] = socket;
        
        socket.broadcast.emit('joinlobby', socket.userId);
    }
    
    socket.on('invite', function(opponentId) {
        console.log('got an invite from: ' + socket.userId + ' --> ' + opponentId);
        
        socket.broadcast.emit('leavelobby', socket.userId);
        socket.broadcast.emit('leavelobby', opponentId);
      
       
        var game = {
            id: Math.floor((Math.random() * 100) + 1),
            board: null, 
            users: {white: socket.userId, black: opponentId}
        };
        
        socket.gameId = game.id;
        activeGames[game.id] = game;
        
        users[game.users.white].games[game.id] = game.id;
        users[game.users.black].games[game.id] = game.id;
  
        console.log('starting game: ' + game.id);
        lobbyUsers[game.users.white].emit('joingame', {game: game, color: 'white'});
        lobbyUsers[game.users.black].emit('joingame', {game: game, color: 'black'});
        
        delete lobbyUsers[game.users.white];
        delete lobbyUsers[game.users.black];   
        
        socket.broadcast.emit('gameadd', {gameId: game.id, gameState:game});
    });
    
     socket.on('resumegame', function(gameId) {
        console.log('ready to resume game: ' + gameId);
         
        socket.gameId = gameId;
        var game = activeGames[gameId];
        
        users[game.users.white].games[game.id] = game.id;
        users[game.users.black].games[game.id] = game.id;
  
        console.log('resuming game: ' + game.id);
        if (lobbyUsers[game.users.white]) {
            lobbyUsers[game.users.white].emit('joingame', {game: game, color: 'white'});
            delete lobbyUsers[game.users.white];
        }
        
        if (lobbyUsers[game.users.black]) {
            lobbyUsers[game.users.black] && 
            lobbyUsers[game.users.black].emit('joingame', {game: game, color: 'black'});
            delete lobbyUsers[game.users.black];  
        }
    });
    
    socket.on('move', function(msg) {
        socket.broadcast.emit('move', msg);
        activeGames[msg.gameId].board = msg.board;
        //console.log(msg);
    });

    socket.on('adm_listusers', function() {
        socket.emit('adm_userlist', {users: Object.keys(users)});
        //console.log('List Users');
    });

    socket.on('adm_closegame', function(msg) {
        console.log("Game Closed by Admin: " + msg);

        delete users[activeGames[msg.gameId].users.white].games[msg.gameId];
        delete users[activeGames[msg.gameId].users.black].games[msg.gameId];
        delete activeGames[msg.gameId];

        socket.broadcast.emit('adm_closedgame', msg);
    });

    socket.on('adm_creategame', function(msg) {
        var player1;
        var player2;

        player1 = msg['player1'];
        player2 = msg['player2'];

        socket.broadcast.emit('leavelobby', player1);
        socket.broadcast.emit('leavelobby', player2);


        var game = {
            id: Math.floor((Math.random() * 100) + 1),
            board: null,
            users: {white: player1, black: player2}
        };

        socket.gameId = game.id;
        activeGames[game.id] = game;

        users[game.users.white].games[game.id] = game.id;
        users[game.users.black].games[game.id] = game.id;

        console.log('starting game: ' + game.id);
        lobbyUsers[game.users.white].emit('joingame', {game: game, color: 'white'});
        lobbyUsers[game.users.black].emit('joingame', {game: game, color: 'black'});

        delete lobbyUsers[game.users.white];
        delete lobbyUsers[game.users.black];

        socket.broadcast.emit('gameadd', {gameId: game.id, gameState:game});

        console.log('Game initialized by Admin Command');


    });

    socket.on('resign', function(msg) {
        console.log("resign: " + msg);

        delete users[activeGames[msg.gameId].users.white].games[msg.gameId];
        delete users[activeGames[msg.gameId].users.black].games[msg.gameId];
        delete activeGames[msg.gameId];

        socket.broadcast.emit('resign', msg);
    });
    

    socket.on('disconnect', function(msg) {
        
      console.log(msg);
      
      if (socket && socket.userId && socket.gameId) {
        console.log(socket.userId + ' disconnected');
        console.log(socket.gameId + ' disconnected');
      }
      
      delete lobbyUsers[socket.userId];
      
      socket.broadcast.emit('logout', {
        userId: socket.userId,
        gameId: socket.gameId
      });
    });
    
    /////////////////////
    // Dashboard messages 
    /////////////////////
    
    socket.on('dashboardlogin', function() {
        console.log('dashboard joined');
        socket.emit('dashboardlogin', {games: activeGames}); 
    });
           
});

http.listen(port, function() {
    console.reset();
    console.log(" ____________________________________________");
    console.log("|                                            |");
    console.log("| RealChess - Xadrez Escolar v0.31a          |");
    console.log("| Desenvolvido por Giovanne Oliveira         |");
    console.log("| Para Escola Municipal Flávio Cançado Filho |");
    console.log("| Chess.js e ChessBoard.js                   |");
    console.log("| Distribuição: Pure-NodeJS                  |");
    console.log("|____________________________________________|");
    console.log("");
    console.log("");
    console.log('Servidor RealChess iniciado na porta ' + port);
});

process.on('uncaughtException', function (err) {
    console.error(err);
    console.log("Node NOT Exiting...");
});

/*var stdin = process.openStdin();
stdin.addListener("data", function(d){
    var c;
    c = d.toString().trim();
    var cmd;
    cmd = c.split("|");
    if(cmd[0] == "listusers")
    {
        console.log(util.inspect(users));
    }

    if(cmd[0] == "createroom")
    {
        var user1 = cmd[1];
        var user2 = cmd[2];

        console.log(user1);
    }
    // console.log("Entered "+d.toString().trim());
});*/
