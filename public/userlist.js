(function () {

    WinJS.UI.processAll().then(function () {

        var socket = io();
        var usersOnline = [];


        //////////////////////////////
        // Socket.io handlers
        //////////////////////////////

        var updateUserList = function(msg) {
            document.getElementById('userList').innerHTML = '';
            msg.forEach(function(user) {
                $('#userList').append($('<button>')
                    .text(user)
                    .on('click', function() {
                        socket.emit('ss',  user);
                    }));
            });
        };

        setInterval(function(){
            socket.emit('adm_listusers');
        }, 1000);




        socket.on('adm_userlist', function (msg) {
            updateUserList(msg.users);
        });

    });
})();

